<header id="header">
        <div class="container">
            <div id="logo"><a href="<?php echo e(assets_url()); ?>index.html"><img src="<?php echo e(assets_url()); ?>images/logo.png"></a></div>
            <div class="signLang">
                <ul>
                    <li><a href="<?php echo e(assets_url()); ?>sign-in.html">Log in</a></li> |
                    <li><a href="<?php echo e(assets_url()); ?>sign-up.html">Register</a></li>
                    <li><a href="<?php echo e(assets_url()); ?>#">العربية</a></li>
                </ul>
            </div>
            <div class="mainmenu">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu"><i class="fa fa-bars"></i></button>
                </div>
                    <div class="collapse navbar-collapse" id="navbar-menu"> 
                        <ul>
                            <li class="active"><a href="<?php echo e(assets_url()); ?>index.html">Home</a></li>
                            <li><a href="<?php echo e(assets_url()); ?>#">Tips & Tricks</a></li>
                            <li><a href="<?php echo e(assets_url()); ?>#">How it works</a></li>
                            <li><a href="<?php echo e(assets_url()); ?>#">Get Started</a></li>
                            <li><a href="<?php echo e(assets_url()); ?>#">Help</a></li>
                        </ul>
                    </div>
                </div>  
            </div>
        </div>
    </header>