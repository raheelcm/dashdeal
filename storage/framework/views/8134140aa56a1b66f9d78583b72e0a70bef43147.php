<?php $__env->startSection('content'); ?>
	<div class="container">
		<div class="row buybids" >
			<h2>1. Choose your Bid Pack</h2>
			<?php $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
			<a href="<?php echo e(url('buybids')); ?>?packID=<?php echo e($item->packID); ?>">
			<div class="col-md-3 col-sm-3">
				<div class="bidbox">
					<div class="bids">
						<h2><?php echo e($item->bids); ?></h2>
						<strong>bids</strong>
						<label><?php echo e($item->per_bid); ?>C<br> Bids!</label>
					</div>
					<p><?php echo e($item->bids); ?> Bids (<?php echo e($item->per_bid); ?>kwd/Bid)</p>
					<strong>Price: <span><?php echo e($item->price); ?></span></strong>
				</div>
			</div>
			</a>
			<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>


			
		</div>
	</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>