<?php $__env->startSection('content'); ?>
<div class="content-wrapper"> 
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1> <?php echo e(trans('labels.AddPackage')); ?> <small><?php echo e(trans('labels.AddPackage')); ?>...</small> </h1>
    <ol class="breadcrumb">
       <li><a href="<?php echo e(URL::to('admin/dashboard/this_month')); ?>"><i class="fa fa-dashboard"></i> <?php echo e(trans('labels.breadcrumb_dashboard')); ?></a></li>
      <li><a href="<?php echo e(URL::to('admin/listingProducts')); ?>"><i class="fa fa-dashboard"></i> <?php echo e(trans('labels.ListingAllProducts')); ?></a></li>
      <li class="active"><?php echo e(trans('labels.AddPackage')); ?></li>
    </ol>
  </section>
  
  <!-- Main content -->
  <section class="content"> 
    <!-- Info boxes --> 
    
    <!-- /.row -->

    <div class="row">
      <div class="col-md-12">
        <div class="box">
          <div class="box-header">
            <h3 class="box-title"><?php echo e(trans('labels.AddNewProduct')); ?> </h3>
          </div>
          
          <!-- /.box-header -->
          <div class="box-body">
            <div class="row">
              <div class="col-xs-12">
                    <div class="box box-info">
                        <!--<div class="box-header with-border">
                          <h3 class="box-title">Edit Product</h3>
                        </div>-->
                        <!-- /.box-header -->
                        <!-- form start -->                        
                         <div class="box-body">
                          <?php if( count($errors) > 0): ?>
                            <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <div class="alert alert-danger" role="alert">
                                    <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                    <span class="sr-only"><?php echo e(trans('labels.Error')); ?>:</span>
                                    <?php echo e($error); ?>

                                </div>
                             <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                          <?php endif; ?>
                          <?php if(isset($package)): ?>
                          <?php echo Form::open(array('url' =>'admin/updatePackage?packID='.$package->packID, 'method'=>'post', 'class' => 'form-horizontal form-validate', 'enctype'=>'multipart/form-data')); ?>

                          <?php else: ?>
                          <?php echo Form::open(array('url' =>'admin/addNewPackage', 'method'=>'post', 'class' => 'form-horizontal form-validate', 'enctype'=>'multipart/form-data')); ?>

                                    
                                    <?php endif; ?>
                        
                            <?php echo Form::open(array('url' =>'admin/addNewPackage', 'method'=>'post', 'class' => 'form-horizontal form-validate', 'enctype'=>'multipart/form-data')); ?>

                            <?php if(isset($package)): ?>

                                    <input type="hidden" name="packID" value="<?php echo e($package->packID); ?>">
                                    <?php endif; ?>
                                <div class="form-group">
                                  <label for="name" class="col-sm-2 col-md-3 control-label">Name</label>
                                  <div class="col-sm-10 col-md-4">
                                    <input type="text" name="name"
                                    <?php if(isset($package)): ?>

                                    value="<?php echo e($package->name); ?>"
                                    <?php endif; ?>

                                     class="form-control field-validate" />
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label for="name" class="col-sm-2 col-md-3 control-label">Bids</label>
                                  <div class="col-sm-10 col-md-4">
                                    <input type="text"
                                    <?php if(isset($package)): ?>

                                    value="<?php echo e($package->bids); ?>"
                                    <?php endif; ?>
                                     name="bids" class="form-control field-validate" />
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label for="name" class="col-sm-2 col-md-3 control-label">Per Bid</label>
                                  <div class="col-sm-10 col-md-4">
                                    <input type="text"
                                      <?php if(isset($package)): ?>

                                    value="<?php echo e($package->per_bid); ?>"
                                    <?php endif; ?>
                                     name="per_bid" class="form-control field-validate" />
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label for="name" class="col-sm-2 col-md-3 control-label">Price</label>
                                  <div class="col-sm-10 col-md-4">
                                    <input type="text"

                                      <?php if(isset($package)): ?>

                                    value="<?php echo e($package->price); ?>"
                                    <?php endif; ?>
                                     name="price" class="form-control field-validate" />
                                  </div>
                                </div>
                              
                                
                              <!-- /.box-body -->
                              <div class="box-footer text-center">
                                <button type="submit" class="btn btn-primary">
                                <?php if(isset($package)): ?>
                                    Update Package
                                    <?php else: ?>
                                    <?php echo e(trans('labels.AddPackage')); ?>

                                    <?php endif; ?></button>
                                <a href="listingProducts" type="button" class="btn btn-default"><?php echo e(trans('labels.back')); ?></a>
                              </div>
                              
                              <!-- /.box-footer -->
                            <?php echo Form::close(); ?>

                        </div>
                  </div>
              </div>
            </div>
            
          </div>
          <!-- /.box-body --> 
        </div>
        <!-- /.box --> 
      </div>
      <!-- /.col --> 
    </div>
    <!-- /.row --> 
    
    <!-- Main row --> 
    
    <!-- /.row --> 
  </section>
  <!-- /.content --> 
</div>
<script src="<?php echo asset('resources/views/admin/plugins/jQuery/jQuery-2.2.0.min.js'); ?>"></script>
<script type="text/javascript">
		$(function () {
			
			//for multiple languages
			
			
			//bootstrap WYSIHTML5 - text editor
			$(".textarea").wysihtml5();
			
    });
</script>
<?php $__env->stopSection(); ?> 
<?php echo $__env->make('admin.layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>