<?php $__env->startSection('content'); ?>
 <div class="container">
        <?php echo $__env->make('includes.banner', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <div class="searchSelect">
            <select>
              <option>Browse Categories </option>
              <option>Option 1</option>
              <option>Option 2</option>
              <option>Option 3</option>
            </select>
            <form class="search">
                <input type="text" name="" placeholder="Search Auctions....">
                <input type="submit" name="" value="Search">
            </form>
        </div>
        <div class="products">
            <div class="row">
                <?php $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$sing): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <?php echo $__env->make('includes.box_1', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                <?php if(($key+1)%6 === 0): ?>
                <div class="clearfix"></div>

<?php endif; ?>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>

        </div>
        <div class="navigation">
            <ul>
                <li class="navPrev"><a href="<?php echo e(assets_url()); ?>#">Prev</a></li>
                <li class="active"><a href="<?php echo e(assets_url()); ?>#">1</a></li>
                <li><a href="<?php echo e(assets_url()); ?>#">2</a></li>
                <li><a href="<?php echo e(assets_url()); ?>#">3</a></li>
                <li><a href="<?php echo e(assets_url()); ?>#">4</a></li>
                <li><a href="<?php echo e(assets_url()); ?>#">5</a></li>
                <li><a href="<?php echo e(assets_url()); ?>#">6</a></li>
                <li><a href="<?php echo e(assets_url()); ?>#">7</a></li>
                <li><a href="<?php echo e(assets_url()); ?>#">8</a></li>
                <li class="navNext"><a href="<?php echo e(assets_url()); ?>#">Next</a></li>

            </ul>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>