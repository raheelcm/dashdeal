<?php $__env->startSection('content'); ?>
<div class="container">
		<h1>Forgot Your Password</h1>
		<div id="alert"></div>
		<div class="formaraa login">
			<form action="" id="forget_form" onsubmit="return forget_pass()">
				  <div class="formBack">
				  	<h2>Forgot Your Password ?</h2>
				  	<p style=" color: #fff; margin: 0 0 10px;">Enter your email and we will send you instructions on how to reset your password</p>
				  	
				  	
					  <div class="form-group">
					    <input type="Email" class="form-control" id="forgt_email" placeholder="Email Address">
					  </div>
					  <input  type="submit" class="btn btn-default" value="Update Password">
				  </div>
			</form> 
			<p>Don’t have a Best Mazad account?<span>Sign up now and start winning</span></p>
		</div>
		
	</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
<script type="text/javascript">
function forget_pass()
{
	var settings = {
  "url": BASE_URL+"/processForgotPassword?customers_email_address="+$('#forgt_email').val(),
  "method": "POST",
  "timeout": 0,
};
$('#alert').html('');
$.ajax(settings).done(function (response) {
var html = "";
var obj = JSON.parse(response);
	console.log(obj);
  if(obj.success == "1")
  {
  	html = '<div class="alert alert-success" role="alert">'+obj['message']+'</div>';
  }
  else
  {
  	html = '<div class="alert alert-danger" role="alert">'+obj['message']+'</div>';
  }
  $('#alert').html(html);
});
return false;
}
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>