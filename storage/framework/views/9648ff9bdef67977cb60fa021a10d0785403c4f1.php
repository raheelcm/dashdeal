<?php $__env->startSection('content'); ?>
<div class="container">
		<div class="row product-page">
			<div class="col-md-6 col-sm-12">
				<div class="productSide">
					<h3>Apple Watch Series 4, Wifi and Cellular</h3>
					<p>Buy it Now Price: KWD 75.000 </p>
					<div class="slider">
						<div class="product-slider">
						  	<div id="carousel" class="carousel slide" data-ride="carousel">
							    <div class="carousel-inner">
							      <div class="item active"> <img src="<?php echo e(url('/')); ?>/<?php echo e($product->products_image); ?>"> </div>
							      <div class="item"> <img src="<?php echo e(assets_url()); ?>images/img14.png"> </div>
							      <div class="item"> <img src="<?php echo e(assets_url()); ?>images/img15.png"> </div>
							      <div class="item"> <img src="<?php echo e(assets_url()); ?>images/img16.png"> </div>
							    </div>
							</div>
						  	<div class="clearfix">
							    <div id="thumbcarousel" class="carousel slide" data-interval="false">
								    <div class="carousel-inner">
								        <div class="item active">
									         <div data-target="#carousel" data-slide-to="1" class="thumb"><img src="<?php echo e(assets_url()); ?>images/img14.png"></div>
									          <div data-target="#carousel" data-slide-to="2" class="thumb"><img src="<?php echo e(assets_url()); ?>images/img15.png"></div>
									          <div data-target="#carousel" data-slide-to="3" class="thumb"><img src="<?php echo e(assets_url()); ?>images/img16.png"></div>
									          
								        </div>
								        
								    </div>
							      <!-- /carousel-inner  
							      <a class="left carousel-control" href="#thumbcarousel" role="button" data-slide="prev"> <i class="fa fa-angle-left" aria-hidden="true"></i> </a> <a class="right carousel-control" href="#thumbcarousel" role="button" data-slide="next"><i class="fa fa-angle-right" aria-hidden="true"></i> </a> </div>
							    <!-- /thumbcarousel --> 
							    
							  	</div>
							</div>
						</div>
					</div>
					<div class="product-table">
						<p>Previous Winner - Blocked From Bidding</p>
						<div class="table-responsive">          
							<table class="table">
							    <thead>
							      <tr>
							        <th>DATE</th>
							        <th>USERNAME</th>
							        <th>PRICE</th>
							        <th>BIDS PLACED</th>
							        <th>EST. TOTAL COST</th>
							      </tr>
							    </thead>
							    <tbody>
							      <tr>
							        <td>12/24/2019</td>
							        <td>USERNAME</td>
							        <td>KWD 3</td>
							        <td>46</td>
							        <td>KWD 10.000</td>
							      </tr>
							      <tr>
							        <td>12/24/2019</td>
							        <td>USERNAME</td>
							        <td>KWD 3</td>
							        <td>46</td>
							        <td>KWD 10.000</td>
							      </tr>
							      <tr>
							        <td>12/24/2019</td>
							        <td>USERNAME</td>
							        <td>KWD 3</td>
							        <td>46</td>
							        <td>KWD 10.000</td>
							      </tr>
							      <tr>
							        <td>12/24/2019</td>
							        <td>USERNAME</td>
							        <td>KWD 3</td>
							        <td>46</td>
							        <td>KWD 10.000</td>
							      </tr>
							      <tr>
							        <td>12/24/2019</td>
							        <td>USERNAME</td>
							        <td>KWD 3</td>
							        <td>46</td>
							        <td>KWD 10.000</td>
							      </tr>
							      <tr>
							        <td>12/24/2019</td>
							        <td>USERNAME</td>
							        <td>KWD 3</td>
							        <td>46</td>
							        <td>KWD 10.000</td>
							      </tr>
							      <tr>
							        <td>12/24/2019</td>
							        <td>USERNAME</td>
							        <td>KWD 3</td>
							        <td>46</td>
							        <td>KWD 10.000</td>
							      </tr>
							    </tbody>
							</table>
						</div>
					</div>	
				</div>
			</div>	
			<div class="col-md-4 col-sm-6">
				<div class="product-table  finalPro">
					<p>Final Sales Price <span>KWD 2.000</span></p>
					<p>Highest Bidder: <span><strong>USERNAME</strong></span></p>
					<hr>
					<div class="member">
						<div class="memberImg">
							<a href="#"><img src="<?php echo e(assets_url()); ?>images/img17.png"></a>
							<p>Description</p>
						</div>
						<div class="memberdetail">
							<p>Member Since: 2018 </p>
							<p>Resident of: Kuwait</p>
						</div>			
					</div>
					<hr>
					<div class="table-responsive">          
						<table class="table">
						    <thead>
						      
						    </thead>
						    <tbody>
						      	<tr>
							        <td>KWD 5.000</td>
							        <td>USERNAME</td>
							        <td>01:42:05 AM</td>
							    </tr>
						      	<tr>
							        <td>KWD 5.000</td>
							        <td>USERNAME</td>
							        <td>01:42:05 AM</td>
							    </tr>
							    <tr>
							        <td>KWD 5.000</td>
							        <td>USERNAME</td>
							        <td>01:42:05 AM</td>
							    </tr>
							    <tr>
							        <td>KWD 5.000</td>
							        <td>USERNAME</td>
							        <td>01:42:05 AM</td>
							    </tr>
							    <tr>
							        <td>KWD 5.000</td>
							        <td>USERNAME</td>
							        <td>01:42:05 AM</td>
							    </tr>
							    <tr>
							        <td>KWD 5.000</td>
							        <td>USERNAME</td>
							        <td>01:42:05 AM</td>
							    </tr>
							    <tr>
							        <td>KWD 5.000</td>
							        <td>USERNAME</td>
							        <td>01:42:05 AM</td>
							    </tr>
							    <tr>
							        <td>KWD 5.000</td>
							        <td>USERNAME</td>
							        <td>01:42:05 AM</td>
							    </tr>
						    </tbody>
						</table>
					</div>
					<h1>00:00:09</h1>
					<span class="span">With each new bid the price goes up KWD 1.000 and timer starts over from 10 seconds.</span>
					<div class="buyNow">
						<strong>Purchase bids to take part in the auctions</strong>
						<button class="buyButton">BUY NOW</button>
					</div>
				</div>
			</div>
			<div class="col-md-2 col-sm-6">
				<div class="productBox">
					<div class="productBack">
						<p>Apple Watch Series 4, Wifi and Cellular </p>
						<a href="#"><img src="<?php echo e(assets_url()); ?>images/img5.png"></a>
					</div>
					<strong>KWD 75.000</strong>
					<p>00:00:00</p>
				</div>
				<div class="productBox">
					<div class="productBack">
						<p>Apple Watch Series 4, Wifi and Cellular </p>
						<a href="#"><img src="<?php echo e(assets_url()); ?>images/img5.png"></a>
					</div>
					<strong>KWD 75.000</strong>
					<p>00:00:00</p>
				</div>
				<div class="productBox">
					<div class="productBack">
						<p>Apple Watch Series 4, Wifi and Cellular </p>
						<a href="#"><img src="<?php echo e(assets_url()); ?>images/img5.png"></a>
					</div>
					<strong>KWD 75.000</strong>
					<p>00:00:00</p>
				</div>
			</div>
		</div>
		<div class="productdisc">
			<h2>Apple Watch Series 4, Wifi and Cellular</h2>
			The Apple Watch 4, or Apple Watch Series 4, arrived on September 21st, 2018, as the successor to the Apple Watch 3. Apple announced the Apple Watch Series 4 smartwatch during the 2018 Apple Special Event in Cupertino, California.

			<strong>What’s New in the Apple Watch 4</strong>

			<p>Compared to the Apple Watch 3, the new Apple Watch 4 offers a faster processor (S4 64-bit dual-core processor), larger screen and louder speaker to
			go along with LTE cellular support, walkie-talkie mode, more extensive and context-based notifications, and more.</p>
			<p>The Apple Watch Series 4 runs on Apple’s WatchOS 5 operating system, and also packs an array of enhanced fitness and health tracking features, 
			including automatic exercise tracking, improved heart rate monitoring and notifications for abnormal heart rates or rhythms, an electrical heart sensor 
			for taking an electrocargiogram (ECG), and the ability to detect if you fall and initiate an emergency call for you.</p>
		</div>
	</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>