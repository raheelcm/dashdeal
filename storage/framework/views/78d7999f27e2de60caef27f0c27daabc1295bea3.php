<?php $__env->startSection('content'); ?>
<div class="container">
		<h1>Login</h1>
		<div class="formaraa login">
			<form action="">
				  <div class="formBack">
				  	<h2>Log in to Best Mazad Market</h2>
				  	<a href="#" class="formBook">Log in with Facebook</a>
				  	<div class="or"><span>OR</span></div>
					  <div class="form-group">
					    <input type="Email" class="form-control" id="" placeholder="Username or Email Address">
					  </div>
					  <div class="form-group">
					    <input type="password" class="form-control" id="pwd" placeholder="Password">
					  </div>
					  <input  type="submit" class="btn btn-default" value="LOG IN">
					  <a href="#" class="forgot">Forgot your password?</a>
				  </div>
			</form> 
			<p>Don’t have a Best Mazad account?<span>Sign up now and start winning</span></p>
		</div>
		
	</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>