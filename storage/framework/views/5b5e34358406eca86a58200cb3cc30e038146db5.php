<footer id="footer">
        <div class="links">
            <div class="row">
                <div class="col-md-4 col-sm-4">
                    <h3>Site Links</h3>
                    <ul>
                        <li><a href="<?php echo e(assets_url()); ?>#">Auctions</a></li>
                        <li><a href="<?php echo e(assets_url()); ?>#">My dashboard</a></li>
                        <li><a href="<?php echo e(assets_url()); ?>#">Buy bids</a></li>
                        <li><a href="<?php echo e(assets_url()); ?>#">BMM Reviews</a></li>
                        <li><a href="<?php echo e(assets_url()); ?>#">BMM is Legit</a></li>
                    </ul>
                </div>
                <div class="col-md-4 col-sm-4">
                    <h3>Help</h3>
                    <ul>
                        <li><a href="<?php echo e(url('/page/how-it-works')); ?>">How it works</a></li>
                        <li><a href="<?php echo e(url('/page/how-to-bid')); ?>">How to bid in an auction</a></li>
                        <li><a href="<?php echo e(url('/page/tips&tricks')); ?>">Tips & Tricks</a></li>
                        <li><a href="<?php echo e(url('/page/whatisabidpack')); ?>">What is a bid pack?</a></li>
                        <li><a href="<?php echo e(url('/page/Whatistimeashighest')); ?>">What is "Time as highest bidder" ?</a></li>
                        <li><a href="<?php echo e(url('/page/Promotions')); ?>">Promotions</a></li>
                        <li><a href="<?php echo e(url('/page/Orders&shipping')); ?>">Orders & Shipping</a></li>
                        <li><a href="<?php echo e(url('/page/Payments')); ?>">Payments</a></li>
                        <li><a href="<?php echo e(url('/page/houserules')); ?>">House Rules</a></li>
                    </ul>
                </div>
                <div class="col-md-4 col-sm-4">
                    <h3>About</h3>
                    <ul>
                        <li><a href="<?php echo e(assets_url()); ?>#">Our team</a></li>
                        <li><a href="<?php echo e(assets_url()); ?>#">Careers</a></li>
                        <li><a href="<?php echo e(assets_url()); ?>#">Our values</a></li>
                        <li><a href="<?php echo e(assets_url()); ?>#">Terms of use</a></li>
                        <li><a href="<?php echo e(assets_url()); ?>#">Privacy policy</a></li>
                        <li><a href="<?php echo e(assets_url()); ?>#">Accessibility</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="footerBottom">
            <p>Download “Best Mazad Market” app</p>
            <div class="app">
                <a href="<?php echo e(assets_url()); ?>#"> <img src="<?php echo e(assets_url()); ?>images/img1.png"></a>
                <a href="<?php echo e(assets_url()); ?>#"> <img src="<?php echo e(assets_url()); ?>images/img2.png"></a>
            </div>
            <a href="<?php echo e(assets_url()); ?>#" class="contact">Contact Us</a>
        </div>
        <div class="copyrights">
            <p>All Rights Reserved, © Best Mazad Market Kuwait </p>
            <span>Powered By <a href="<?php echo e(assets_url()); ?>https://channelsmedia.com/" target="_blank">Channels Media Kuwait</a></span>
        </div>
    </footer>