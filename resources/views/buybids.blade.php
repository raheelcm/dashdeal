@extends('layouts.master')
@section('content')
	<div class="container">
		<div class="row buybids" >
			<h2>1. Choose your Bid Pack</h2>
			@foreach ($products as $item)
			<a href="{{url('buybids')}}?packID={{$item->packID}}">
			<div class="col-md-3 col-sm-3">
				<div class="bidbox">
					<div class="bids">
						<h2>{{$item->bids}}</h2>
						<strong>bids</strong>
						<label>{{$item->per_bid}}C<br> Bids!</label>
					</div>
					<p>{{$item->bids}} Bids ({{$item->per_bid}}kwd/Bid)</p>
					<strong>Price: <span>{{$item->price}}</span></strong>
				</div>
			</div>
			</a>
			@endforeach


			
		</div>
	</div>
@endsection