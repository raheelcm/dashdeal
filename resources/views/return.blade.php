<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
 
<!------ Include the above in your HEAD tag ---------->

<div class="container">
	<div class="row text-center">
        <div class="col-sm-6 col-sm-offset-3">
       
       
   
      
        
 

@if($status = 1)

 <br><br> <h2 style="color:#0fad00">Success</h2>
       <img src="{{url('public/iconfinder_f-check_256_282474.png')}}">
         
        <p style="font-size:20px;color:#5C5C5C;">Payment Success.</p>

@else

 <br><br> <h2 style="color:#0fad00">Failed!!</h2>
        <img src="{{url('public/iconfinder_f-cross_256_282471.png')}}">
         
        <p style="font-size:20px;color:#5C5C5C;">Please Try Again</p>

@endif

  </div>
	</div>
</div>
<script>
         setTimeout(function(){
            window.location.href = '<?= url('/') ?>';
         }, 5000);
      </script>