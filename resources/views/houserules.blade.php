@extends('layouts.master')
@section('content')
<div class="container">
		<h1>Help</h1>
		<div class="row">
			<div class="col-md-4 col-sm-4">
				<div class="sidebar">
					<ul>
                        <li ><a href="{{url('/page/how-it-works')}}">> How it works</a></li>
                        <li><a href="{{url('/page/how-to-bid')}}">> How to bid in an auction</a></li>
                        <li><a href="{{url('/page/tips&tricks')}}">> Tips & Tricks</a></li>
                        <li><a href="{{url('/page/whatisabidpack')}}">> What is a bid pack?</a></li>
                        <li><a href="{{url('/page/Whatistimeashighest')}}">> What is "Time as highest bidder" ?</a></li>
                        <li><a href="{{url('/page/Promotions')}}">> Promotions</a></li>
                        <li><a href="{{url('/page/Orders&shipping')}}">> Orders & Shipping</a></li>
                        <li><a href="{{url('/page/Payments')}}">> Payments</a></li>
                        <li class="active"><a href="{{url('/page/houserules')}}">> House Rules</a></li>
                    </ul>
				</div>
			</div>
			<div class="col-md-8 col-sm-8">
				<div class="details">
					<h2>House Rules</h2>
					<h3>Account policy</h3>
					<p>Each user may have or maintain control over only one account per person, no exceptions! Each household may only have or maintain control over a maximum of two accounts.</p>

					<h3>Reselling</h3>
					<p>In order for us to offer such amazing deals to our customers, it is not permitted to resell anything obtained from DealDash. Violating this will result in account closure.</p>

					<h3>Winning limits</h3>
					<p>To ensure equal chances for everyone, you may win:</p>

					<p>1: The same item only once per 30 days; all remaining BidBuddies on other auctions for the same item will be automatically cancelled at the time of your win.
					<br>2: Up to 50 auctions per week; all remaining BidBuddies on other auctions will be automatically cancelled at the time of your 50th win. You can bid again when the limit resets at 00:00 PST on the day of the week you created your account.

					<h3>Please plan your bidding strategy with these limits in mind.</h3>

					<p>Some auctions are restricted only to users who have never won a "One-Per-User" auction for that product before. These auctions are marked as "One-Per-User" on the auction page. Winning a regular auction does not restrict bidding in "One-Per-User" auctions of the same product.</p>

					<h3>Team play</h3>
					<p>It is strictly forbidden to bid in the same auction with your family members, friends, or acquaintances. Any attempts at this type of collusion may make other bidders feel like they're not bidding on a level playing field.</p>

					<p>DealDash is using sophisticated software, monitoring systems and safety protection measures to detect bidders who are not complying with this policy. Bidders found to be participating in teams will have all their auction wins voided, and in severe or repeat instances such users will be permanently suspended from the auctions without a chance to receive refund.</p>

					<p>Please note that while DealDash works hard to shut down any accounts engaging in team play as soon as possible, bids will not be reimbursed to other bidders involved in auctions where team play has taken place. To ensure that accounts engaging in team play are shut down as fast as possible, DealDash encourages bidders to report any suspected team players to the customer service team.</p>

					<h3>Use of third party bidding software</h3>
					<p>The use of any kind of external bidding software is strictly forbidden. DealDash will under its discretion void auction wins and shut down accounts caught using such tools to bid on the website.</p>

					<h3>Usernames</h3>
					<p>So that people do not become confused or upset your username may not be obscene or misleading ("DealDash admin", "Auction closed"..) or chosen with the clear purpose of impersonating an existing user.</p>

					<h3>Employees and their immediate family members</h3>
					<p>DealDash employees and their family members (defined as parents, spouse, siblings and children) and any person residing in the same household as employees may not under any circumstances participate in DealDash auctions.</p>
				</div>
			</div>
		</div>
		
	</div>
@endsection