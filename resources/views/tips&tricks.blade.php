@extends('layouts.master')
@section('content')
<div class="container">
		<h1>Help</h1>
		<div class="row">
			<div class="col-md-4 col-sm-4">
				<div class="sidebar">
					<ul>
                        <li ><a href="{{url('/page/how-it-works')}}">> How it works</a></li>
                        <li ><a href="{{url('/page/how-to-bid')}}">> How to bid in an auction</a></li>
                        <li class="active"><a href="{{url('/page/tips&tricks')}}">> Tips & Tricks</a></li>
                        <li><a href="{{url('/page/whatisabidpack')}}">> What is a bid pack?</a></li>
                        <li><a href="{{url('/page/Whatistimeashighest')}}">> What is "Time as highest bidder" ?</a></li>
                        <li ><a href="{{url('/page/Promotions')}}">> Promotions</a></li>
                        <li ><a href="{{url('/page/Orders&shipping')}}">> Orders & Shipping</a></li>
                        <li ><a href="{{url('/page/Payments')}}">> Payments</a></li>
                        <li ><a href="{{url('/page/houserules')}}">> House Rules</a></li>
                    </ul>
				</div>
			</div>
			<div class="col-md-8 col-sm-8">
				<div class="details">
					<h2>Tips & Tricks</h2>
					<div class="tipbox">
						<div class="tipImg"><a href="#"><img src="{{assets_url()}}images/img7.png"></a></div>
						<h3>Conserve Your Bids</h3>
						<p>Bidding rapidly will only waste your bids and inflate the auction price. Bid strategically, or book BidBuddy to do it for you.</p>
					</div>
					<div class="tipbox">
						<div class="tipImg"><a href="#"><img src="{{assets_url()}}images/img8.png"></a></div>
						<h3>Earn Free Bids</h3>
						<p>When you are the Highest Bidder, you will earn time towards your next level. When you reach a new level, you receive free bids.</p>
					</div>
					<div class="tipbox">
						<div class="tipImg"><a href="#"><img src="{{assets_url()}}images/img9.png"></a></div>
						<h3>Study The Competition</h3>
						<p>Who else is bidding on the item? Watch and learn. Some people keep a notepad to track bidders and what type of bidding strategies they are using.</p>
					</div>
					<div class="tipbox">
						<div class="tipImg"><a href="#"><img src="{{assets_url()}}images/img10.png"></a></div>
						<h3>Check Out The Winners</h3>
						<p>The winners page reveals recently won items and the final auction prices.</p>
					</div>
					<div class="tipbox">
						<div class="tipImg"><a href="#"><img src="{{assets_url()}}images/img11.png"></a></div>
						<h3>Try Different Times</h3>
						<p>Auctions might be easier to win at different times of the day. Auctions are only open to the US.</p>
					</div>
					<div class="tipbox">
						<div class="tipImg"><a href="#"><img src="{{assets_url()}}images/img12.png"></a></div>
						<h3>On a Budget?</h3>
						<p>Try bidding on less-expensive, less-popular items. There will likely be less competition.</p>
					</div>
					
				</div>
			</div>
		</div>
		
	</div>
@endsection