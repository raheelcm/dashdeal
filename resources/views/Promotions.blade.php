@extends('layouts.master')
@section('content')
<div class="container">
		<h1>Help</h1>
		<div class="row">
			<div class="col-md-4 col-sm-4">
				<div class="sidebar">
					<ul>
                        <li ><a href="{{url('/page/how-it-works')}}">> How it works</a></li>
                        <li ><a href="{{url('/page/how-to-bid')}}">> How to bid in an auction</a></li>
                        <li><a href="{{url('/page/tips&tricks')}}">> Tips & Tricks</a></li>
                        <li><a href="{{url('/page/whatisabidpack')}}">> What is a bid pack?</a></li>
                        <li><a href="{{url('/page/Whatistimeashighest')}}">> What is "Time as highest bidder" ?</a></li>
                        <li class="active"><a href="{{url('/page/Promotions')}}">> Promotions</a></li>
                        <li ><a href="{{url('/page/Orders&shipping')}}">> Orders & Shipping</a></li>
                        <li ><a href="{{url('/page/Payments')}}">> Payments</a></li>
                        <li ><a href="{{url('/page/houserules')}}">> House Rules</a></li>
                    </ul>
				</div>
			</div>
			<div class="col-md-8 col-sm-8">
				<div class="details">
					<h2>Promotions</h2>
					<img src="{{assets_url()}}images/img6.png">
					<p>This picture is an example. Check out the promotion banner on the front page or bid purchase page to see the currently active promotions.</p>

					<p>DealDash has a set of changing promotions that give you extra benefits. Check out the promotion banner on the front page or bid purchase page to see the currently active promotions.</p>
					<h3>Free bids faster</h3>
					<p>Each time you are the highest bidder, your bidder bar fills up. When the bar is full, you can claim your free bids and your level goes up. During “Free bids faster” promotions the bar fills up faster, meaning that you will earn the next level and the free bids faster!</p>

					<h3>50% Off</h3>
					<p>On auctions that start during the “50% Off” promotion you have to pay only 50% of the final auction price when you win! And shipping on DealDash is always free!</p>

					<h3>Auctions starting at the same time</h3>
					<p>More excitement, more deals. The timer counts down to the starting time of the auctions. Make sure to be online early enough to check out all the auctions before they start to make sure you find the most interesting products.</p>

					<h3>Bid pricing</h3>
					<p>The bid price may change daily depending on the active offers. Make sure to check out the current offer often, not to miss out on the cheapest days.</p>

					<h3>Auction leaderboards</h3>
					<p>When the Auction leaderboard promotion is active, the three bidders who have collected the most Time As Highest Bidder in an auction will earn additional bid rewards!</p>

					<h3>Every BidBuddy Counts</h3>
					<p>Every time your BidBuddy places a bid you receive 9 seconds on your Time as Highest Bidder progress bar, even if someone places a bid right after you. If any Time as Highest Bidder multipliers are in place, those will also be applied.</p>

					<h3>Daily winning limits</h3>
					<p>Each bidder will be able to win a maximum of 5 auctions each day when this promotion is active. Once bidders win their 5 items, they won't be able to enter more auctions until the end of the day, which means less competition for bidders who still haven't hit their limit!</p>

					<h3>Mystery auctions</h3>
					<p>Mystery auctions are marked with a question mark. The item is revealed 5 minutes before the auction starts, and no BidBuddies can be booked before that. This means that there is likely to be less competition in these auctions.</p>

					<h3>No exchange auctions</h3>
					<p>Unlike most auctions, auctions marked with a red "no exchange" icon cannot be exchanged for bids by the winner. This means that everyone participating in one of these auctions is in it to purchase the product.</p>
				</div>
			</div>
		</div>
		
	</div>
@endsection