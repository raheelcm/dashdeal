@extends('layouts.master')
@section('content')
<div class="container">
		<h1>Help</h1>
		<div class="row">
			<div class="col-md-4 col-sm-4">
				<div class="sidebar">
					<ul>
                        <li ><a href="{{url('/page/how-it-works')}}">> How it works</a></li>
                        <li ><a href="{{url('/page/how-to-bid')}}">> How to bid in an auction</a></li>
                        <li ><a href="{{url('/page/tips&tricks')}}">> Tips & Tricks</a></li>
                        <li class="active"><a href="{{url('/page/whatisabidpack')}}">> What is a bid pack?</a></li>
                        <li><a href="{{url('/page/Whatistimeashighest')}}">> What is "Time as highest bidder" ?</a></li>
                        <li ><a href="{{url('/page/Promotions')}}">> Promotions</a></li>
                        <li ><a href="{{url('/page/Orders&shipping')}}">> Orders & Shipping</a></li>
                        <li ><a href="{{url('/page/Payments')}}">> Payments</a></li>
                        <li ><a href="{{url('/page/houserules')}}">> House Rules</a></li>
                    </ul>
				</div>
			</div>
			<div class="col-md-8 col-sm-8">
				<div class="details">
					<h2>What is a bid pack?</h2>
					<h3>What is a bid pack?</h3>
					<p>A bid pack is what you need to buy to get started with bidding. DealDash operates in a pay to play model so before you can take part in an auction you need to buy bids. As soon as you have bids in your account you can start bidding.</p>

					<h3>How do I get bids?</h3>
					<p>You obtain bids by purchasing bid packs. Bid packs come in various sizes, such as 200 bids. Each time you place a bid, one bid is removed from your bid balance. When you run out of bids, you can either buy more bids, or use the "Buy it Now" option to get your bids back from an auction!</p>

					<h3>How do I bid?</h3>
					<p>Buy some bids and use them in the auctions! Each bid increases the auction price by 1 cent. The auction continues until nobody places a bid within a 10 second time period. When you win an auction, simply pay the winning price and the item will be delivered within 14 days!</p>
				</div>
			</div>
		</div>
		
	</div>
@endsection