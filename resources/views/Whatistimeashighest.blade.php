@extends('layouts.master')
@section('content')
<div class="container">
		<h1>Help</h1>
		<div class="row">
			<div class="col-md-4 col-sm-4">
				<div class="sidebar">
					<ul>
                        <li ><a href="{{url('/page/how-it-works')}}">> How it works</a></li>
                        <li ><a href="{{url('/page/how-to-bid')}}">> How to bid in an auction</a></li>
                        <li ><a href="{{url('/page/tips&tricks')}}">> Tips & Tricks</a></li>
                        <li ><a href="{{url('/page/whatisabidpack')}}">> What is a bid pack?</a></li>
                        <li class="active"><a href="{{url('/page/Whatistimeashighest')}}">> What is "Time as highest bidder" ?</a></li>
                        <li ><a href="{{url('/page/Promotions')}}">> Promotions</a></li>
                        <li ><a href="{{url('/page/Orders&shipping')}}">> Orders & Shipping</a></li>
                        <li ><a href="{{url('/page/Payments')}}">> Payments</a></li>
                        <li ><a href="{{url('/page/houserules')}}">> House Rules</a></li>
                    </ul>
				</div>
			</div>
			<div class="col-md-8 col-sm-8">
				<div class="details">
					<h2>What is "Time as highest bidder"?</h2>
					<h3>We reward you for bidding</h3>
					<p>Each time you are the highest bidder, your bidder bar fills up. When the bar is full, you can claim your free bids and your level goes up. The higher your level, the more free bids you earn.</p>
				</div>
			</div>
		</div>
		
	</div>
@endsection