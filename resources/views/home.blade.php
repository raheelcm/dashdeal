@extends('layouts.master')
@section('content')
 <div class="container">
        @include('includes.banner')
        <div class="searchSelect">
            <select>
              <option>Browse Categories </option>
              <option>Option 1</option>
              <option>Option 2</option>
              <option>Option 3</option>
            </select>
            <form class="search">
                <input type="text" name="" placeholder="Search Auctions....">
                <input type="submit" name="" value="Search">
            </form>
        </div>
        <div class="products">
            <div class="row">
                @foreach ($products as $key=>$sing)
                @include('includes.box_1')
                @if (($key+1)%6 === 0)
                <div class="clearfix"></div>

@endif
                @endforeach
            </div>

        </div>
        <div class="navigation">
            <ul>
                <li class="navPrev"><a href="{{assets_url()}}#">Prev</a></li>
                <li class="active"><a href="{{assets_url()}}#">1</a></li>
                <li><a href="{{assets_url()}}#">2</a></li>
                <li><a href="{{assets_url()}}#">3</a></li>
                <li><a href="{{assets_url()}}#">4</a></li>
                <li><a href="{{assets_url()}}#">5</a></li>
                <li><a href="{{assets_url()}}#">6</a></li>
                <li><a href="{{assets_url()}}#">7</a></li>
                <li><a href="{{assets_url()}}#">8</a></li>
                <li class="navNext"><a href="{{assets_url()}}#">Next</a></li>

            </ul>
        </div>
    </div>
@endsection