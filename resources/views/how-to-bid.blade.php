@extends('layouts.master')
@section('content')
<div class="container">
		<h1>Help</h1>
		<div class="row">
			<div class="col-md-4 col-sm-4">
				<div class="sidebar">
					<ul>
                        <li ><a href="{{url('/page/how-it-works')}}">> How it works</a></li>
                        <li class="active"><a href="{{url('/page/how-to-bid')}}">> How to bid in an auction</a></li>
                        <li><a href="{{url('/page/tips&tricks')}}">> Tips & Tricks</a></li>
                        <li><a href="{{url('/page/whatisabidpack')}}">> What is a bid pack?</a></li>
                        <li><a href="{{url('/page/Whatistimeashighest')}}">> What is "Time as highest bidder" ?</a></li>
                        <li><a href="{{url('/page/Promotions')}}">> Promotions</a></li>
                        <li><a href="{{url('/page/Orders&shipping')}}">> Orders & Shipping</a></li>
                        <li><a href="{{url('/page/Payments')}}">> Payments</a></li>
                        <li ><a href="{{url('/page/houserules')}}">> House Rules</a></li>
                    </ul>
				</div>
			</div>
			<div class="col-md-8 col-sm-8">
				<div class="details">
					<h2>How to bid in an auction</h2>
					<p>1: Find the product you like<br>
					2: Use the “Bid Now” Button or the BidBuddy to bid automatically for you.<br>
					3: If you are the highest bidder when the timer hits zero, you WIN!</p>

					<h3>Things to note</h3>
					<p>Get Bids Back with Buy it Now<br>
					Lost an auction? No worries! You can always purchase the item at the Buy it Now price. It will be delivered within 14 days. What about all of the bids you placed in the auction? You get them all back! Every time you use the "Buy it Now" option, every bid you placed in that auction is returned to your bid balance, so you can try again in new auctions without buying more bids!</p>

					<h3>Save with BidBuddy</h3>
					<p>Don't want to have your smartphone in your hand all day long? No problem! The BidBuddy will place bids on your behalf. Simply enter the number of bids you wish to place in the auction in the "BidBuddy" box, and your BidBuddy will automatically place these bids one at a time. It won't waste your bids - the BidBuddy only places bids at the most optimal times in the auction, ensuring you get as many free bids as possible! See "Time as Highest Bidder" for more info.</p>

					<p>Booking a BidBuddy before an auction starts does not guarantee participation in the auction. Particularly in popular auctions BidBuddies by some users may not have a possibility to place a bid before the No Jumper limit is reached. In these cases all bids booked into the BidBuddy are returned automatically to the user. To secure a spot in an auction, DealDash advises to book the BidBuddy or place one manual bid after the auction has started and before it has reached the No Jumper limit.</p>

					<h3>Free Bids with "Time as Highest Bidder"</h3>
					<p>At DealDash we like to reward you for bidding. Every time you are the highest bidder in the auction, you will accumulate some time.</p>

					<p>You will see the Time as Highest Bidder meter at the bottom of your screen. When this fills up, you get free bids! How high can you level up?</p>

					<h3>Winning Limits</h3>
					<p>To ensure equal chances for everyone, you may win:</p>

					<p>1: The same item only once per 30 days; all remaining BidBuddies on other auctions for the same item will be automatically cancelled at the time of your win.<br>
					2: Up to 50 auctions per week; all remaining BidBuddies on other auctions will be automatically cancelled at the time of your 50th win. You can bid again when the limit resets at 00:00 PST on the day of the week you created your account.</p>
				</div>
			</div>
		</div>
		
	</div>
@endsection