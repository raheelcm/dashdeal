@extends('layouts.master')
@section('content')
<div class="container">
		<h1>Help</h1>
		<div class="row">
			<div class="col-md-4 col-sm-4">
				<div class="sidebar">
					<ul>
                        <li ><a href="{{url('/page/how-it-works')}}">> How it works</a></li>
                        <li ><a href="{{url('/page/how-to-bid')}}">> How to bid in an auction</a></li>
                        <li><a href="{{url('/page/tips&tricks')}}">> Tips & Tricks</a></li>
                        <li><a href="{{url('/page/whatisabidpack')}}">> What is a bid pack?</a></li>
                        <li><a href="{{url('/page/Whatistimeashighest')}}">> What is "Time as highest bidder" ?</a></li>
                        <li><a href="{{url('/page/Promotions')}}">> Promotions</a></li>
                        <li class="active"><a href="{{url('/page/Orders&shipping')}}">> Orders & Shipping</a></li>
                        <li><a href="{{url('/page/Payments')}}">> Payments</a></li>
                        <li ><a href="{{url('/page/houserules')}}">> House Rules</a></li>
                    </ul>
				</div>
			</div>
			<div class="col-md-8 col-sm-8">
				<div class="details">
					<h2>Orders & Shipping</h2>
					<h3>Tracking information</h3>
					<p>When your item has shipped, tracking information will be made available on your My Orders page, allowing you to view the tracking information for any recent orders you have made.</p>

					<h3>Where's my item?</h3>
					<p>Have you checked your "My Orders" page for tracking information? If no tracking information is available, please allow 10 business days from the day you made the payment for this to update. If you believe there is a problem with your order, contact our support team. Please note that we do not have tracking information available for gift cards valued under $250.</p>

					<h3>Ships by dates</h3>
					<p>Auctions with "Ships by" dates are pre-sale items that are not yet ready for shipment. The date indicated on the auction page is an estimate of when the item will be shipped out from our distribution center. You should receive your item within 10 business days of this date. If you haven't, please let us know and we'll be happy to help.</p>

					<h3>Returns</h3>
					<p>If you wish to return an item, please contact support within 7 days of receiving the item.</p>

					<p>If the item is damaged or defective upon receipt, or does not otherwise match the product description, we will replace it for you at no cost.</p>

					<h3>Are items brand new?</h3>
					<p>Yes! We work with top suppliers such as Walmart and Sears, who ship the items directly to your door! If you are unhappy with any item you receive, simply contact our support team and they will help you out.</p>

					<h3>Do you ship to Canada or Australia?</h3>
					<p>Unfortunately not. We ship exclusively to the USA (not AK and HI).</p>
				</div>
			</div>
		</div>
		
	</div>
@endsection