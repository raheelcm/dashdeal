@extends('admin.layout')
@section('content')
<div class="content-wrapper"> 
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1> {{ trans('labels.Products') }} <small>Listing All Packges...</small> </h1>
    <ol class="breadcrumb">
       <li><a href="{{ URL::to('admin/dashboard/this_month') }}"><i class="fa fa-dashboard"></i> {{ trans('labels.breadcrumb_dashboard') }}</a></li>
      <li class="active"> {{ trans('labels.Products') }}</li>
    </ol>
  </section>
  
  <!-- Main content -->
  <section class="content"> 
    <!-- Info boxes --> 
    
    <!-- /.row -->

    <div class="row">
      <div class="col-md-12">
        <div class="box">
          <div class="box-header">
            <h3 class="box-title">Listing All Packges </h3>
            <div class="box-tools pull-right">
            	<a href="addPackage" type="button" class="btn btn-block btn-primary">Add new Packges</a>
            </div>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
          	
            <div class="row">
              <div class="col-xs-12">
                @if (session('success'))
    <div class="alert alert-success alert-dismissible" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    {!! session('success') !!}
    </div>
@endif          @if (session('error'))
    <div class="alert alert-danger alert-dismissible" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    {!! session('error') !!}
    </div>
@endif


				  @if (count($errors) > 0)
					  @if($errors->any())
						<div class="alert alert-success alert-dismissible" role="alert">
						  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						  {{$errors->first()}}
						</div>
					  @endif
				  @endif
              </div>
            </div>
            
            <div class="row">
            	<div class="col-xs-12"><br><br><br>

             </div>
            
            <div class="row">
              <div class="col-xs-12">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th>{{ trans('labels.ID') }}</th>
                      <th>Detail</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                   <tbody>
                   @if(count($results['products'])>0)
                    @foreach ($results['products'] as  $key=>$product)
                    	   <tr>
                            <td>{{ $key+1 }}</td>
                            <td width="45%">
                              <b>Name :</b>{{ $product->name }}<br>
                              <b>Bids :</b>{{ $product->bids }}<br>
                              <b>Per Bid :</b>{{ $product->per_bid }}<br>
                              <b>Price :</b>{{ $product->price }}<br>
                            </td>
                           
                            <td>
                            <a href="deletePackage?packID={{$product->packID}}" class="btn btn-danger">Delete Package</a> 
                            <a href="updatePackage?packID={{$product->packID}}" class="btn btn-primary">Edit Package</a>
                            </td>
                          </tr>
                     @endforeach
                   @else
                   		<tr>
                            <td colspan="5">{{ trans('labels.NoRecordFound') }}</td>
                       </tr>
                   @endif 
                  </tbody>
                </table>
                <div class="col-xs-12 text-right">
                </div>
              </div>
              
            </div>
          </div>
          <!-- /.box-body --> 
        </div>
        <!-- /.box --> 
      </div>
      <!-- /.col --> 
    </div>
    
    <!-- deleteProductModal -->
	<div class="modal fade" id="deleteProductModal" tabindex="-1" role="dialog" aria-labelledby="deleteProductModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="deleteProductModalLabel">{{ trans('labels.DeleteProduct') }}</h4>
		  </div>
		  {!! Form::open(array('url' =>'admin/deleteProduct', 'name'=>'deleteProduct', 'id'=>'deleteProduct', 'method'=>'post', 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data')) !!}
				  {!! Form::hidden('action',  'delete', array('class'=>'form-control')) !!}
				  {!! Form::hidden('products_id',  '', array('class'=>'form-control', 'id'=>'products_id')) !!}
		  <div class="modal-body">						
			  <p>{{ trans('labels.DeleteThisProductDiloge') }}?</p>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('labels.Close') }}</button>
			<button type="submit" class="btn btn-primary" id="deleteProduct">{{ trans('labels.DeleteProduct') }}</button>
		  </div>
		  {!! Form::close() !!}
		</div>
	  </div>
	</div>
    <!-- /.row --> 
    
    <!-- Main row --> 
    
    <!-- /.row --> 
  </section>
  <!-- /.content --> 
</div>
@endsection 