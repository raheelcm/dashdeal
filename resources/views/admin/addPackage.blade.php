@extends('admin.layout')
@section('content')
<div class="content-wrapper"> 
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1> {{ trans('labels.AddPackage') }} <small>{{ trans('labels.AddPackage') }}...</small> </h1>
    <ol class="breadcrumb">
       <li><a href="{{ URL::to('admin/dashboard/this_month') }}"><i class="fa fa-dashboard"></i> {{ trans('labels.breadcrumb_dashboard') }}</a></li>
      <li><a href="{{ URL::to('admin/listingProducts')}}"><i class="fa fa-dashboard"></i> {{ trans('labels.ListingAllProducts') }}</a></li>
      <li class="active">{{ trans('labels.AddPackage') }}</li>
    </ol>
  </section>
  
  <!-- Main content -->
  <section class="content"> 
    <!-- Info boxes --> 
    
    <!-- /.row -->

    <div class="row">
      <div class="col-md-12">
        <div class="box">
          <div class="box-header">
            <h3 class="box-title">{{ trans('labels.AddNewProduct') }} </h3>
          </div>
          
          <!-- /.box-header -->
          <div class="box-body">
            <div class="row">
              <div class="col-xs-12">
                    <div class="box box-info">
                        <!--<div class="box-header with-border">
                          <h3 class="box-title">Edit Product</h3>
                        </div>-->
                        <!-- /.box-header -->
                        <!-- form start -->                        
                         <div class="box-body">
                          @if( count($errors) > 0)
                            @foreach($errors->all() as $error)
                                <div class="alert alert-danger" role="alert">
                                    <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                    <span class="sr-only">{{ trans('labels.Error') }}:</span>
                                    {{ $error }}
                                </div>
                             @endforeach
                          @endif
                          @if(isset($package))
                          {!! Form::open(array('url' =>'admin/updatePackage?packID='.$package->packID, 'method'=>'post', 'class' => 'form-horizontal form-validate', 'enctype'=>'multipart/form-data')) !!}
                          @else
                          {!! Form::open(array('url' =>'admin/addNewPackage', 'method'=>'post', 'class' => 'form-horizontal form-validate', 'enctype'=>'multipart/form-data')) !!}
                                    
                                    @endif
                        
                            {!! Form::open(array('url' =>'admin/addNewPackage', 'method'=>'post', 'class' => 'form-horizontal form-validate', 'enctype'=>'multipart/form-data')) !!}
                            @if(isset($package))

                                    <input type="hidden" name="packID" value="{{$package->packID}}">
                                    @endif
                                <div class="form-group">
                                  <label for="name" class="col-sm-2 col-md-3 control-label">Name</label>
                                  <div class="col-sm-10 col-md-4">
                                    <input type="text" name="name"
                                    @if(isset($package))

                                    value="{{$package->name}}"
                                    @endif

                                     class="form-control field-validate" />
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label for="name" class="col-sm-2 col-md-3 control-label">Bids</label>
                                  <div class="col-sm-10 col-md-4">
                                    <input type="text"
                                    @if(isset($package))

                                    value="{{$package->bids}}"
                                    @endif
                                     name="bids" class="form-control field-validate" />
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label for="name" class="col-sm-2 col-md-3 control-label">Per Bid</label>
                                  <div class="col-sm-10 col-md-4">
                                    <input type="text"
                                      @if(isset($package))

                                    value="{{$package->per_bid}}"
                                    @endif
                                     name="per_bid" class="form-control field-validate" />
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label for="name" class="col-sm-2 col-md-3 control-label">Price</label>
                                  <div class="col-sm-10 col-md-4">
                                    <input type="text"

                                      @if(isset($package))

                                    value="{{$package->price}}"
                                    @endif
                                     name="price" class="form-control field-validate" />
                                  </div>
                                </div>
                              
                                
                              <!-- /.box-body -->
                              <div class="box-footer text-center">
                                <button type="submit" class="btn btn-primary">
                                @if(isset($package))
                                    Update Package
                                    @else
                                    {{ trans('labels.AddPackage') }}
                                    @endif</button>
                                <a href="listingProducts" type="button" class="btn btn-default">{{ trans('labels.back') }}</a>
                              </div>
                              
                              <!-- /.box-footer -->
                            {!! Form::close() !!}
                        </div>
                  </div>
              </div>
            </div>
            
          </div>
          <!-- /.box-body --> 
        </div>
        <!-- /.box --> 
      </div>
      <!-- /.col --> 
    </div>
    <!-- /.row --> 
    
    <!-- Main row --> 
    
    <!-- /.row --> 
  </section>
  <!-- /.content --> 
</div>
<script src="{!! asset('resources/views/admin/plugins/jQuery/jQuery-2.2.0.min.js') !!}"></script>
<script type="text/javascript">
		$(function () {
			
			//for multiple languages
			
			
			//bootstrap WYSIHTML5 - text editor
			$(".textarea").wysihtml5();
			
    });
</script>
@endsection 