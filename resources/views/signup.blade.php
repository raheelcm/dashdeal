@extends('layouts.master')
@section('content')
<div class="container">
		<h1>Registration</h1>
		<div class="formaraa">
			 <form action=" {{url('/register')}}" method="POST">
			 	@if(session('message'))
			 	<div class="alert alert-success">
				{{ session('message') }}
			</div>
				@endif

				  <div class="formBack">
				  	<h2>Register to Best Mazad Market</h2>
					  <div class="form-group">
					    <label for="">Username</label>
					    <input type="text" class="form-control" id="" placeholder="Pick a username." name="user_name">
					  </div>
					  <div class="form-group">
					    <label for="">Email</label>
					    <input type="Email" class="form-control" id="" placeholder="We won’t share it." name="customers_email_address">
					  </div>
					  <div class="form-group">
					    <label for="pwd">Password</label>
					    <input type="password" class="form-control" id="pwd" placeholder="At least 4 characters." name="customers_password">
					  </div>
				  </div>
				  <div class="checkbox">
				    <label><input type="checkbox"> I have read and agree to Best Mazad’s <a href="#">Terms of Use</a> and <a href="#"> Privacy Policy</a>. I also agree that I am at least 18 years of age and to only open one account per person.</label>
				  </div>
				  <input  type="submit" class="btn btn-default" value="REGISTER">
			</form> 
		</div>
	</div>
@endsection