@extends('layouts.master')
@section('content')
<div class="container">
		<h1>Help</h1>
		<div class="row">
			<div class="col-md-4 col-sm-4">
				<div class="sidebar">
					<ul>
                        <li ><a href="{{url('/page/how-it-works')}}">> How it works</a></li>
                        <li ><a href="{{url('/page/how-to-bid')}}">> How to bid in an auction</a></li>
                        <li><a href="{{url('/page/tips&tricks')}}">> Tips & Tricks</a></li>
                        <li><a href="{{url('/page/whatisabidpack')}}">> What is a bid pack?</a></li>
                        <li><a href="{{url('/page/Whatistimeashighest')}}">> What is "Time as highest bidder" ?</a></li>
                        <li><a href="{{url('/page/Promotions')}}">> Promotions</a></li>
                        <li ><a href="{{url('/page/Orders&shipping')}}">> Orders & Shipping</a></li>
                        <li class="active"><a href="{{url('/page/Payments')}}">> Payments</a></li>
                        <li ><a href="{{url('/page/houserules')}}">> House Rules</a></li>
                    </ul>
				</div>
			</div>
			<div class="col-md-8 col-sm-8">
				<div class="details">
					<h2>Payments</h2>
					<h3>Payment methods</h3>
					<p>You can pay by credit card, debit card or via PayPal. We accept all Visa, MasterCard, American Express and Discover credit cards, Visa debit cards including Visa Electron as well as Maestro debit cards. We use SSL encryption to make sure that the transactions are 100% secure.</p>

					<h3>Do I have to pay sales tax?</h3>
					<p>Orders may be subject to sales tax (when applicable and required by law). For Buy it Now orders the tax is based on the Buy it Now price and for auction wins the tax is based on the final auction price.</p>

					<h3>Can not finish a payment?</h3>
					<p>If you receive an error message when making a payment, try making the payment again. You may have made a slight error when entering the billing zip code. If the problem persists, try an alternative payment method such as PayPal. PayPal will accept your card, even if you do not have a PayPal account. Our support team can also help you out.</p>
					<a href="#">CONTACT OUR SUPPORT TEAM</a>
					<h3>Is there a subscription fee?</h3>
					<p>No, there is no subscription fee and you only pay for the bids you buy</p>
				</div>
			</div>
		</div>
		
	</div>
@endsection