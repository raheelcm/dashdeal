@extends('layouts.master')
@section('content')
<div class="container">
		<h1>Help</h1>
		<div class="row">
			<div class="col-md-4 col-sm-4">
				<div class="sidebar">
					<ul>
                        <li class="active"><a href="{{url('/page/how-it-works')}}">> How it works</a></li>
                        <li><a href="{{url('/page/how-to-bid')}}">> How to bid in an auction</a></li>
                        <li><a href="{{url('/page/tips&tricks')}}">> Tips & Tricks</a></li>
                        <li><a href="{{url('/page/whatisabidpack')}}">> What is a bid pack?</a></li>
                        <li><a href="{{url('/page/Whatistimeashighest')}}">> What is "Time as highest bidder" ?</a></li>
                        <li><a href="{{url('/page/Promotions')}}">> Promotions</a></li>
                        <li><a href="{{url('/page/Orders&shipping')}}">> Orders & Shipping</a></li>
                        <li><a href="{{url('/page/Payments')}}">> Payments</a></li>
                        <li><a href="{{url('/page/houserules')}}">> House Rules</a></li>
                    </ul>
				</div>
			</div>
			<div class="col-md-8 col-sm-8">
				<div class="details">
					<h2>How it works</h2>
					<p>1. Each bid raises the price by $0.01.<br>
					2. The auction clock restarts from 10 seconds every time someone bids.<br>
					3. If no new bids are placed before the clock runs out, the last bidder wins.<br>
					4. Before you can take part in an auction you need to buy bids. Bids are currently on sale for <br>
					13¢ each! This small fee makes the massive 95% off savings possible!</p>
					<div class="formaraa in-details">
					 <form action="">
						  <div class="formBack">
						  	<h2>Start Saving Today</h2>
						  	<strong>Sign up information</strong>
							  <div class="form-group">
							    <label for="">Pick a username.</label>
							    <input type="text" class="form-control" id="" placeholder="Username">
							  </div>
							  <div class="form-group">
							    <label for="">Your Email Address</label>
							    <input type="Email" class="form-control" id="" placeholder="Email">
							  </div>
							  <div class="form-group">
							    <label for="pwd"></label>
							    <input type="password" class="form-control" id="pwd" placeholder="password">
							    <a href="#">Have a promo code? (Optional).</a>
							  </div>
						 
						  <div class="checkbox">
						    <label><input type="checkbox"> I have read and agree to Best Mazad’s <a href="#">Terms of Use</a> and <a href="#"> Privacy Policy</a>. I also agree that I am at least 18 years of age and to only open one account per person.</label>
						  </div>
						  <hr>
						  <input  type="submit" class="btn btn-default" value="Next">
						   </div>
					</form> 
				</div>
				</div>
			</div>
		</div>
		
	</div>
@endsection