@extends('layouts.master')
@section('content')
<div class="container">
		<h1>Login</h1>
		<div class="formaraa login">
			<form action=" {{url('/login')}}" method="POST">
			 	@if(session('error'))
			 	<div class="alert alert-danger">
				{{ session('error') }}
			</div>
				@endif
				  <div class="formBack">
				  	<h2>Log in to Best Mazad Market</h2>
				  	<a href="#" class="formBook">Log in with Facebook</a>
				  	<div class="or"><span>OR</span></div>
					  <div class="form-group">
					    <input type="Email" class="form-control" id="" placeholder="Username or Email Address" name="customers_email_address">
					  </div>
					  <div class="form-group">
					    <input type="password" class="form-control" id="pwd" placeholder="Password" name="customers_password">
					  </div>
					  <input  type="submit" class="btn btn-default" value="LOG IN">
					  <a href="{{url('/page/forgot-password')}}" class="forgot">Forgot your password?</a>
				  </div>
			</form> 
			<p>Don’t have a Best Mazad account?<span>Sign up now and start winning</span></p>
		</div>
		
	</div>
@endsection