<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>BMM</title>
    <link rel="stylesheet" type="text/css" href="{{assets_url()}}css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="{{assets_url()}}css/bootstrap.min.css">
    <link rel="stylesheet" href="{{assets_url()}}css/owl.carousel.min.css">
    <link rel="stylesheet" type="text/css" href="{{assets_url()}}css/owl.theme.default.min.css">
    <link rel="stylesheet" type="text/css" href="{{assets_url()}}css/style.css">
</head>
<body>
<div id="wrapper">
        @include('includes.header')

                @yield('content')
    @include('includes.footer')
</div>
<script type="text/javascript" src="{{assets_url()}}js/jquery.js"></script>
<script type="text/javascript" src="{{assets_url()}}js/bootstrap.min.js"></script>
<script type="text/javascript" src="{{assets_url()}}js/owl.carousel.min.js"></script>
<script type="text/javascript">
  var BASE_URL = "{{url('/')}}";
</script>
@yield('script')
<script>
    $(document).ready(function() {
      $('.owl-carousel').owlCarousel({
        loop: true,
        responsiveClass: true,
        responsive: {
          0: {
            items: 1,
            nav: true
          },
          600: {
            items: 1,
            nav: false
          },
          1000: {
            items: 1,
            nav: true,
            loop: false
          }
        }
      })
    })
  </script>

<script>
    $(document).ready(function() {
      $('.owl-carousel2').owlCarousel({
        loop: true,
        responsiveClass: true,
        margin:25,
        responsive: {
          0: {
            items: 1,
            nav: true
          },
          600: {
            items: 3,
            nav: false
          },
          1000: {
            items: 5,
            nav: true,
            loop: false
          }
        }
      })
    })
  </script></body>
</html>