<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Hash;
use Session;
use Redirect;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Crypt;


class PageController extends Controller
{
    public function payment_response($type, $order_id)
    {
    	$orders = DB::table('orders')->where('orders_id', $order_id)->first();
    	if($_GET['response'] == 'SUCCESSS')
			{
				if($type == 'pack')
        {
        	$customers_id = $orders->customers_id;
        	$pack_id = $orders->pack_id;
        	$package = DB::table('packages')->where('packID', $pack_id)->first();
        	$bids = $package->bids;
        	$customer = DB::table('customers')->where('customers_id', $customers_id)->first();
        	$nbid = $customer->bids+ $bids;
        	DB::table('customers')->where('customers_id', $customers_id)->update([
					'bids' => $nbid	
				]);
        	$date_added	=	date('Y-m-d h:i:s');
        	$orders_history_id = DB::table('orders_status_history')->insertGetId(
				[	 'orders_id'  => $order_id,
					 'orders_status_id' => 2,
					 'date_added'  => $date_added,
					 'customer_notified' =>'1',
					 'comments'  =>  'Done payment from channelsmedia gateway'
				]);
            return view('return',array('status'=>1));
        }
				
			}
			else
			{
				return view('return',array('status'=>0));
			}
        
    }
    private function payment_link($data)
	{
$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => "https://knet.gingergulf.com//api/payment_link?api_key=7f00ac303caa92bd9b447d14e3840c90",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "POST",
  CURLOPT_POSTFIELDS => "data=".json_encode($data),
  CURLOPT_HTTPHEADER => array(
    "Content-Type: application/x-www-form-urlencoded",
    "Accept: application/json",
    "authorization: key"
  ),
));

$response = curl_exec($curl);

curl_close($curl);
return json_decode($response,true);

	}
	public function buybids()
	{
	    $uid = 0;
        
            $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";
        $uid = 0;
        
            $value = session('LoginUser');
            // dd($value);
        if(!empty($value)) 
        {
      $uid =  $value->customers_id;
         } else{
                return redirect('page/login?rurl='.$actual_link);

            }
            $user = DB::table('customers')->where('customers_id', $uid)->where('isActive', '1')->first();
		
		/*if(isset($_GET['response']))
		{
			if($_GET['response'] == 'SUCCESSS')
			{
				return view('return',array('status'=>1));
			}
			else
			{
				return view('return',array('status'=>0));
			}
		}*/
		if(isset($_GET['packID']))
		{
			$package = DB::table('packages')->where('packID', $_GET['packID'])->first();
		    $price = $package->price;

		    $date_added								=	date('Y-m-d h:i:s');
		    $orders_id = DB::table('orders')->insertGetId(
				[	 'customers_id' => $uid,
					 'customers_name'  => $user->customers_email_address,
					 'customers_telephone' => $user->customers_telephone,
					 'customers_email_address'  => $user->customers_email_address,
					 'date_purchased'=>$date_added,
					 'order_price'=> $price,
					 'pack_id'=>$_GET['packID'],
				]);
		    $calll_back = url('/').'/payment_response/pack/'.$orders_id;
		    // $calll_back = 'http://channelsmedia.net/Mazaad-Market/buybids';
		    $name = 'Mazaad customer';
		    if(!empty($user->customers_firstname) && !empty($user->customers_lastname))
		    	$name = $user->customers_firstname.' '.$user->customers_lastname;
		    $phone = '123456789';
		    if(!empty($user->customers_telephone))
		    	$phone = $user->customers_telephone;
		    $pdata = array (
						  'InvoiceValue' => $price,
						  'CustomerName' => $name,
						  'CustomerBlock' => 'Block',
						  'CustomerStreet' => 'Street',
						  'CustomerHouseBuildingNo' => 'Building no',
						  'CustomerCivilId' => '123456789124',
						  'CustomerAddress' => 'Payment Address',
						  'CustomerReference' => '\'.$t.\'',
						  'DisplayCurrencyIsoAlpha' => 'KWD',
						  'CountryCodeId' => '+965',
						  'CustomerMobile' => $phone,
						  'CustomerEmail' => $user->customers_email_address,
						  'DisplayCurrencyId' => 3,
						  'SendInvoiceOption' => 1,
						  'InvoiceItemsCreate' => 
						  array (
						    0 => 
						    array (
						      'ProductId' => NULL,
						      'ProductName' => $package->name.'  Bids Package ID'.$_GET['packID'],
						      'Quantity' => 1,
						      'UnitPrice' => $price,
						    ),
						  ),
						  'CallBackUrl' => $calll_back,
						  'Language' => 2,
						  'ExpireDate' => '2022-12-31T13:30:17.812Z',
						  'ApiCustomFileds' => 'weight=10,size=L,lenght=170',
						  'ErrorUrl' => $calll_back,
						);
				$orders_history_id = DB::table('orders_status_history')->insertGetId(
				[	 'orders_id'  => $orders_id,
					 'orders_status_id' => 1,
					 'date_added'  => $date_added,
					 'customer_notified' =>'1',
					 'comments'  =>  'Packages buy from web'
				]);
			$ret = $this->payment_link($pdata);

			if($ret['status'] == 1)
			{
				// print_r();
				header("Location: ".$ret['link']);
				exit;
			}
		}
		
		$data = array();
    	$lang = cur_lang();
    	$data['products'] = DB::table('packages')->where('status',1)->get();
     	// dd($data['product']->products_image);
    	return view('buybids',$data);

	}
	public function product_detail($id)
	{
		$data = array();
    	$lang = cur_lang();
    	// $data['banners'] = DB::table('banners')->where('banners_group', 'web')->get();
    	$data['product'] = $this->getSingleProducts($id);
    	$data['images'] = $this->productImages($id);
    	$data['related'] = $this->getRelatedProducts($data['product']->categories_id ,$id);
     	// dd($data['product']->products_image);
    	return view('detail',$data);
	}
    public function home()
    {
    	
    	$data = array();
    	$lang = cur_lang();
    	$category_id = 0;
    	if(isset($_GET['category']))
    	{
    		$category_id = $_GET['category'];
    	}

    	$search = '';
    	if(isset($_GET['search']))
    	{
    		$search = $_GET['search'];
    	}
    	$data['banners'] = DB::table('banners')->where('banners_group', 'web')->get();
    	$data['products'] = $this->getProducts($lang,$category_id,$search);
    	$data['categories'] = $this->product_categories();
    	$data['category_select'] = $category_id;
    	$data['search_keyword'] = $search;
     	// dd($data['products'][0]);
    	return view('home',$data);
    }

    public function getProducts($lang ,$category_id,$search)
    {
			
		$language_id     =   cur_lang();		
		$products = DB::table('products_to_categories')
			->leftJoin('categories', 'categories.categories_id', '=', 'products_to_categories.categories_id')
			->leftJoin('categories_description', 'categories_description.categories_id', '=', 'products_to_categories.categories_id')
			->leftJoin('products', 'products.products_id', '=', 'products_to_categories.products_id')
			->leftJoin('products_description','products_description.products_id','=','products.products_id')
			->leftJoin('manufacturers','manufacturers.manufacturers_id','=','products.manufacturers_id')
			->leftJoin('manufacturers_info','manufacturers.manufacturers_id','=','manufacturers_info.manufacturers_id')
			->LeftJoin('specials', function ($join) {
				$join->on('specials.products_id', '=', 'products.products_id')->where('status', '=', '1');
			 })
			->select('products_to_categories.*', 'categories_description.categories_name','categories.*', 'products.*','products_description.*','manufacturers.*','manufacturers_info.manufacturers_url', 'specials.specials_id', 'specials.products_id as special_products_id', 'specials.specials_new_products_price as specials_products_price', 'specials.specials_date_added as specials_date_added', 'specials.specials_last_modified as specials_last_modified', 'specials.expires_date')
			->where('products_description.language_id','=', $language_id)
			->where('categories_description.language_id','=', $language_id)
			->orderBy('products.products_id', 'DESC');
			if($category_id != 0 )
			{
				
				$products = $products->where('products_to_categories.categories_id','=', $category_id);
			}

			if($search != '' )
			{
					
				$products = $products->where('products_description.products_name', 'LIKE', '%' . $search . '%');
			}

			$products = $products->get();
			return $products;
			
		
	}
	//single product
	public function getSingleProducts($products_id){
		
		$language_id     =   cur_lang();
		$products = DB::table('products_to_categories')
			->leftJoin('categories', 'categories.categories_id', '=', 'products_to_categories.categories_id')
			->leftJoin('categories_description', 'categories_description.categories_id', '=', 'products_to_categories.categories_id')
			->leftJoin('products', 'products.products_id', '=', 'products_to_categories.products_id')
			->leftJoin('products_description','products_description.products_id','=','products.products_id')
			->leftJoin('manufacturers','manufacturers.manufacturers_id','=','products.manufacturers_id')
			->leftJoin('manufacturers_info','manufacturers.manufacturers_id','=','manufacturers_info.manufacturers_id')
			->LeftJoin('specials', function ($join) {
				$join->on('specials.products_id', '=', 'products.products_id')->where('status', '=', '1');
			 })
			->select('products_to_categories.*', 'categories_description.categories_name','categories.*', 'products.*','products_description.*','manufacturers.*','manufacturers_info.manufacturers_url', 'specials.specials_id', 'specials.products_id as special_products_id', 'specials.specials_new_products_price as specials_products_price', 'specials.specials_date_added as specials_date_added', 'specials.specials_last_modified as specials_last_modified', 'specials.expires_date')
			->where('products_description.language_id','=', $language_id)
			->where('categories_description.language_id','=', $language_id)
			->where('products.products_id','=', $products_id)
			->first();
			
		return $products;
	}
	public function productImages($products_id)
	{

		return $products_images = DB::table('products_images')->select('image')->where('products_id','=', $products_id)->orderBy('sort_order', 'ASC')->get();
	}

	public function product_categories()
	{
		$language_id     =   cur_lang();
		return  DB::table('categories')
				->leftJoin('categories_description', 'categories_description.categories_id', '=', 'categories.categories_id')
				->where('categories_description.language_id','=', $language_id)
				->orderBy('sort_order', 'ASC')->get();
	}

	public function getRelatedProducts($Cate_id,$pID){
		
		$language_id     =   cur_lang();		
		$products = DB::table('products_to_categories')
			->leftJoin('categories', 'categories.categories_id', '=', 'products_to_categories.categories_id')
			->leftJoin('categories_description', 'categories_description.categories_id', '=', 'products_to_categories.categories_id')
			->leftJoin('products', 'products.products_id', '=', 'products_to_categories.products_id')
			->leftJoin('products_description','products_description.products_id','=','products.products_id')
			->leftJoin('manufacturers','manufacturers.manufacturers_id','=','products.manufacturers_id')
			->leftJoin('manufacturers_info','manufacturers.manufacturers_id','=','manufacturers_info.manufacturers_id')
			->LeftJoin('specials', function ($join) {
				$join->on('specials.products_id', '=', 'products.products_id')->where('status', '=', '1');
			 })
			->select('products_to_categories.*', 'categories_description.categories_name','categories.*', 'products.*','products_description.*','manufacturers.*','manufacturers_info.manufacturers_url', 'specials.specials_id', 'specials.products_id as special_products_id', 'specials.specials_new_products_price as specials_products_price', 'specials.specials_date_added as specials_date_added', 'specials.specials_last_modified as specials_last_modified', 'specials.expires_date')
			->where('products_description.language_id','=', $language_id)
			->where('categories_description.language_id','=', $language_id)
			->where('products_to_categories.categories_id','=', $Cate_id)
			->where('products.products_id','!=', $pID)
			->orderBy('products.products_id', 'DESC')
			->get();
			
		return $products;
	}
    //Login
    public function processLogin(Request $request){
		
		$customers_email_address = $request->customers_email_address;
		$customers_password      = $request->customers_password;
		
		$existUser = DB::table('customers')->where('customers_email_address', $customers_email_address)->where('isActive', '1')->get();
		$chk = 0;
		if(count($existUser)>0){
				// dd($existUser);
				if(Hash::check($customers_password, $existUser[0]->customers_password))
				{
					$chk = 1;
				}
			}
			if($chk){


			$customers_id = $existUser[0]->customers_id;
			
			//update record of customers_info			
			$existUserInfo = DB::table('customers_info')->where('customers_info_id', $customers_id)->get();
			$customers_info_id 							= $customers_id;
			$customers_info_date_of_last_logon  		= date('Y-m-d H:i:s');
			$customers_info_number_of_logons     		= '1';
			$customers_info_date_account_created 		= date('Y-m-d H:i:s');
			$global_product_notifications 				= '1';
			
			if(count($existUserInfo)>0){
				//update customers_info table
				DB::table('customers_info')->where('customers_info_id', $customers_info_id)->update([
					'customers_info_date_of_last_logon' => $customers_info_date_of_last_logon,
					'global_product_notifications' => $global_product_notifications,
					'customers_info_number_of_logons'=> DB::raw('customers_info_number_of_logons + 1')
				]);
				
			}else{
				//break;
				//insert customers_info table
				$customers_default_address_id = DB::table('customers_info')->insertGetId(
					 ['customers_info_id' => $customers_info_id,
					  'customers_info_date_of_last_logon' => $customers_info_date_of_last_logon,
					  'customers_info_number_of_logons' => $customers_info_number_of_logons,
					  'customers_info_date_account_created' => $customers_info_date_account_created,
					  'global_product_notifications' => $global_product_notifications
					 ]
				);	
				
				DB::table('customers')->where('customers_id', $customers_id)->update([
					'customers_default_address_id' => $customers_default_address_id	
				]);
			}		
			
			//check if already login or not
			$already_login = DB::table('whos_online')->where('customer_id', '=', $customers_id)->get();
									
			if(count($already_login)>0){
				DB::table('whos_online')
					->where('customer_id', $customers_id)
					->update([
							'full_name'  => $existUser[0]->customers_firstname.' '.$existUser[0]->customers_lastname,
							'time_entry'   => date('Y-m-d H:i:s'),							
					]);
			}else{
				DB::table('whos_online')
					->insert([
							'full_name'  => $existUser[0]->customers_firstname.' '.$existUser[0]->customers_lastname,
							'time_entry' => date('Y-m-d H:i:s'),
							'customer_id'    => $customers_id							
					]);
			}			
			
			//get liked products id
			$products = DB::table('liked_products')->select('liked_products_id as products_id')
			->where('liked_customers_id', '=', $customers_id)
			->get();
			
			if(count($products)>0){
				$liked_products = $products;
			}else{
				$liked_products = array();
			}
			
			$existUser[0]->liked_products = $products;
			
			
			/*$responseData = array('success'=>'1', 'data'=>$existUser, 'message'=>'Data has been returned successfully!');
			$userResponse = json_encode($responseData);*/
			$request->session()->put('LoginUser', $existUser[0]);
			                    if($request->rurl)
                               {
                                return Redirect::to($request->rurl);
                               }
                               else
                               {
                                return redirect('/');
                               }
			// Session::set('LoginUser', $existUser);

			
		}else{
			$existUser = array();
			// $responseData = array('success'=>'0', 'data'=>$existUser, 'message'=>);
			$message = "Invalid email or password";
			return redirect('page/login')->with('error',$message);
		}			
	}
	 public function logout(Request $request) {
        $request->session()->flush();
        session_unset();
        return redirect('/');


    }
    //registration 
	public function processRegistration(Request $request){
		
		$user_name            		=   $request->user_name;
		// $customers_firstname            		=   $request->customers_firstname;
		// $customers_lastname           			=   $request->customers_lastname;			
		$customers_email_address    		    =   $request->customers_email_address;
		$customers_password          		    =   Hash::make($request->customers_password);	
		// $customers_telephone        		    =   $request->customers_telephone;
		$customers_picture        		   		=   '';
		$customers_info_date_account_created 	=   date('y-m-d h:i:s');	
		
		
		if(!empty($customers_picture)){
			$image = substr($customers_picture, strpos($customers_picture, ",") + 1);
			$img = base64_decode($image);
			$dir="resources/assets/images/user_profile/";
			if (!file_exists($dir) and !is_dir($dir)) {
				mkdir($dir);
			} 
			$uploadfile = $dir."/pic_".time().".jpg";
			file_put_contents(base_path().'/'.$uploadfile, $img);
			$profile_photo=$uploadfile;
		}else{
			$profile_photo="resources/assets/images/user_profile/default_user.svg";	
		}
		
		//check email existance
		$existUser = DB::table('customers')->where('customers_email_address', $customers_email_address)->get();	
		
		if(count($existUser)=="1"){	
			//response if email already exit	
			$postData = array();
			$responseData = array('success'=>'0', 'data'=>$postData, 'message'=>"Email address is already exist");
			$userResponse = json_encode($responseData);
			print $userResponse;
		}else{
			$customer_data = array(
				'user_name'			 =>  $user_name,
				// 'customers_firstname'			 =>  $customers_firstname,
				// 'customers_lastname'			 =>  $customers_lastname,
				// 'customers_telephone'			 =>  $customers_telephone,
				'customers_email_address'		 =>  $customers_email_address,
				'customers_password'			 =>  $customers_password,
				'customers_picture'				 =>  $profile_photo,
				'isActive'						 =>  '1',
				'created_at'					 =>	 time()
			);
							
			//insert data into customer
			$customers_id = DB::table('customers')->insertGetId($customer_data);
			
			$userData = DB::table('customers')->where('customers_id', '=', $customers_id)->get();
			
			$responseData = array('success'=>'1', 'data'=>$userData, 'message'=>"Sign Up successfully!");
			$message = "Sign Up successfully!";
			return redirect('page/signup')->with('message',$message);

					
		}	
	}
    public function page($page)
    {
    	// dd($page);
    	return view($page);
    }
}

