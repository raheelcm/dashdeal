<?php

/*
Project Name: IonicEcommerce
Project URI: http://ionicecommerce.com
Author: VectorCoder Team
Author URI: http://vectorcoder.com/
Version: 2.2
*/

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

//validator is builtin class in laravel
use Validator;
use App;
use Lang;

use DB;
//for password encryption or hash protected
use Hash;
use App\Administrator;

//for authenitcate login data
use Auth;

//for requesting a value 
use Illuminate\Http\Request;


class PackageController extends Controller
{
	
	
	
	public function login(){
		if (Auth::check()) {
		  // The user is logged in...
		  return redirect('/admin/dashboard/this_month');
		}else{
			$title = array('pageTitle' => Lang::get("labels.login_page_name"));
			return view("admin.login",$title);
		}
	}
	
	public function admininfo(){
		$administor = administrators::all();		
		return view("admin.login",$title);
	}
	
	//login function
	public function checkLogin(Request $request){
		$validator = Validator::make(
			array(
					'email'    => $request->email,
					'password' => $request->password
				), 
			array(
					'email'    => 'required | email',
					'password' => 'required',
				)
		);
		//check validation
		if($validator->fails()){
			return redirect('admin/login')->withErrors($validator)->withInput();
		}else{
			//check authentication of email and password
			$adminInfo = array("email" => $request->email, "password" => $request->password);
			if(Auth::attempt($adminInfo)) {
				$admin = Auth::User();
				$administrators = DB::table('administrators')->where('myid', $admin->myid)->get();	
				return redirect()->intended('admin/dashboard/this_month')->with('administrators', $administrators);
			}else{
				return redirect('admin/login')->with('loginError',Lang::get("labels.EmailPasswordIncorrectText"));
			}
		}
		
	}
	
	
	//logout
	public function logout(){
		Auth::logout();	
		return redirect()->intended('admin/login');
	}
	
	//admin profile
	public function adminProfile(Request $request){
		$title = array('pageTitle' => Lang::get("labels.Profile"));		
		
		$result = array();
		
		$countries = DB::table('countries')->get();
		$zones = DB::table('zones')->where('zone_country_id', '=', Auth::user()->country)->get();
		
		$result['countries'] = $countries;
		$result['zones'] = $zones;
		
		return view("admin.adminProfile",$title)->with('result', $result);
	}
	
	//updateProfile
	public function updateProfile(Request $request){
		
		$updated_at	= date('y-m-d h:i:s');
		if($request->hasFile('newImage')){
			$image = $request->newImage;
			$fileName = time().'.'.$image->getClientOriginalName();
			$image->move('resources/views/admin/images/admin_profile/', $fileName);
			$uploadImage = 'resources/views/admin/images/admin_profile/'.$fileName; 
		}	else{
			$uploadImage = $request->oldImage;
		}	
		
		$orders_status = DB::table('administrators')->where('myid','=', Auth::user()->myid)->update([
				'user_name'		=>	$request->user_name,
				'first_name'	=>	$request->first_name,
				'last_name'		=>	$request->last_name,
				'address'		=>	$request->address,
				'city'			=>	$request->city,
				'state'			=>	$request->state,
				'zip'			=>	$request->zip,
				'country'		=>	$request->country,
				'phone'			=>	$request->phone,
				'image'			=>	$uploadImage,
				'updated_at'	=>	$updated_at
				]);
		
		$message = Lang::get("labels.ProfileUpdateMessage");
		return redirect()->back()->withErrors([$message]);
	}
	
	//updateProfile
	public function updateAdminPassword(Request $request){
		
		$orders_status = DB::table('administrators')->where('myid','=', Auth::user()->myid)->update([
				'password'		=>	Hash::make($request->password)
				]);
		
		$message = Lang::get("labels.PasswordUpdateMessage");
		return redirect()->back()->withErrors([$message]);
	}

}
