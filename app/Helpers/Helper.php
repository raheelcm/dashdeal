<?php
 
if (!function_exists('human_file_size')) {
    /**
     * Returns a human readable file size
     *
     * @param integer $bytes
     * Bytes contains the size of the bytes to convert
     *
     * @param integer $decimals
     * Number of decimal places to be returned
     *
     * @return string a string in human readable format
     *
     * */
    function human_file_size($bytes, $decimals = 2)
    {
        $sz = 'BKMGTPE';
        $factor = (int)floor((strlen($bytes) - 1) / 3);
        return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . $sz[$factor];
 
    }
}
 
if (!function_exists('assets_url')) {
function assets_url(){
    return url('/').'/public/front/';
}
} 
if (!function_exists('cur_lang')) {
function cur_lang(){
    return '1';
}
} 
if (!function_exists('send_mail')) {
function send_mail($to, $html, $sub) {

        $data = array (

              'personalizations' =>
              array (

                0 => 

                array (

                  'to' => 

                  array (

                    0 => 

                    array (

                      'email' => $to,

                    ),

                  ),

                  'subject' => $sub,

                ),

              ),

              'from' => 

              array (

                'email' => 'mazaad@channelsmedia.net',

                'name' => 'mazaad bazar market',

              ),

              'content' => 

              array (

                0 => 

                array (

                  'type' => 'text/html',

                  'value' => $html,

                ),

              ),

            );

            

            $curl = curl_init();

            

              curl_setopt_array($curl, array(

              CURLOPT_URL => "https://api.sendgrid.com/v3/mail/send",

              CURLOPT_RETURNTRANSFER => true,

              CURLOPT_ENCODING => "",

              CURLOPT_MAXREDIRS => 10,

              CURLOPT_TIMEOUT => 30,

              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,

              CURLOPT_CUSTOMREQUEST => "POST",

              CURLOPT_POSTFIELDS => json_encode($data),

              CURLOPT_HTTPHEADER => array(

                "authorization: Bearer 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",

                "cache-control: no-cache",

                "content-type: application/json",

                "postman-token: 500e002d-9ecb-48a8-eb26-9e81cba79900"

              ),

            ));

            

            $response = curl_exec($curl);

            $err = curl_error($curl);

            

            curl_close($curl);

            

            if ($err) {

              echo $err;


            } else {

              $arr = json_decode($response);
              print_r($arr);

            }

    }
}
if (!function_exists('in_arrayi')) {
 
    /**
     * Checks if a value exists in an array in a case-insensitive manner
     *
     * @param mixed $needle
     * The searched value
     *
     * @param $haystack
     * The array
     *
     * @param bool $strict [optional]
     * If set to true type of needle will also be matched
     *
     * @return bool true if needle is found in the array,
     * false otherwise
     */
    function in_arrayi($needle, $haystack, $strict = false)
    {
        return in_array(strtolower($needle), array_map('strtolower', $haystack), $strict);
    }
}